﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Perineometrico.ViewModels
{
    public class SecondViewModel:BaseViewModel
    {
        public Command StartCommand { get; set; }

        public SecondViewModel()
        {
            StartCommand = new Command(ExecuteStartCommand);
        }

        private async void ExecuteStartCommand()
        {
            await PushAsync<ResultViewModel>();
        }
    }
}
