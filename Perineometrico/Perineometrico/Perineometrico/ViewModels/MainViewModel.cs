﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Perineometrico.ViewModels
{
    public class MainViewModel: BaseViewModel
    {
        public Command ConnectCommand { get; set; }

        public MainViewModel()
        {
            ConnectCommand = new Command(ExecuteConnectCommand);
        }

        private async void ExecuteConnectCommand()
        {
            await PushAsync<SecondViewModel>();
        }
    }
}
