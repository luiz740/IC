﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Perineometrico.ViewModels
{
    public class ResultViewModel: BaseViewModel
    {
        public Command InformationCommand { get; set; }

        public ResultViewModel()
        {
            InformationCommand = new Command(ExecuteInformationCommand);
        }

        private async void ExecuteInformationCommand()
        {
            await PushAsync<InformationViewModel>();
        }
    }
}
