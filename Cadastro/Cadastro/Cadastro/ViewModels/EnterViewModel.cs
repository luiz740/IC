﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Cadastro.ViewModels
{
    public class EnterViewModel: BaseViewModel
    {
        public Command LeaveCommand { get; set; }

        public EnterViewModel()
        {
            LeaveCommand = new Command(ExecuteLeaveCommand);
        }

        private async void ExecuteLeaveCommand()
        {
            await PushAsync<MainViewModel>();
        }
    }
}
