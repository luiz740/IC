﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Cadastro.ViewModels
{
    public class LoginViewModel: BaseViewModel
    {
        public Command RegisterEnterCommand { get; set; }

        public LoginViewModel()
        {
            RegisterEnterCommand = new Command(ExecuteRegisterEnterCommand);
        }

        private async void ExecuteRegisterEnterCommand()
        {
            await DisplayAlert("Mensagem", "Bem-Vindo", "Ok!");
            await PushAsync<EnterViewModel>();
        }
    }
}
