﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Cadastro.ViewModels
{
    public class MainViewModel: BaseViewModel
    {
        public Command EnterCommand { get; set; }
        public Command RegisterCommand { get; set; }

        public MainViewModel()
        {
            EnterCommand = new Command(ExecuteEnterCommand);
            RegisterCommand = new Command(ExecuteRegisterCommand);

        }

        private async void ExecuteRegisterCommand()
        {
            await PushAsync<RegisterViewModel>();
        }

        private async void ExecuteEnterCommand()
        {
            await DisplayAlert("Mensagem", "Bem-Vindo", "Ok!");
            await PushAsync<EnterViewModel>();
        }
    }
}
