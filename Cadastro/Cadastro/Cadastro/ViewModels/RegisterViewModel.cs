﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Cadastro.ViewModels
{
    public class RegisterViewModel: BaseViewModel
    {
        public Command LoginCommand { get; set; }

        public RegisterViewModel()
        {
            LoginCommand = new Command(ExecuteLoginCommand);
        }

        private async void ExecuteLoginCommand()
        {
            await DisplayAlert("Mensagem", "Cadastrado", "Ok!");
            await PushAsync<LoginViewModel>();
        }
    }
}
