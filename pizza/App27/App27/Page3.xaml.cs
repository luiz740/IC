﻿using App27.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App27
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public MyViewModel vm { get; set; }

        public Page3()
        {
            InitializeComponent();

            vm = new MyViewModel();

            BindingContext = vm;
        }
    }
}