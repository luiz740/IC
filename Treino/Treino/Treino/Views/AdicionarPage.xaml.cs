﻿using Treino.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Treino.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdicionarPage : ContentPage
    {
        public AdicionarPage()
        {
            InitializeComponent();
            BindingContext = new AdicionarViewModel();
        }
    }
}