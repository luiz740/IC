﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Treino.ViewModels;

namespace Treino.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TaskPage : TabbedPage
    {
        public TaskPage()
        {
            InitializeComponent();

            BindingContext = new TaskViewModel();
        }
    }
}