﻿namespace Treino.Models
{
    internal class DataPoint
    {
        private double v1;
        private double v2;

        public DataPoint(double v1, double v2)
        {
            this.v1 = v1;
            this.v2 = v2;
        }
    }
}