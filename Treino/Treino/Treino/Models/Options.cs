﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treino.Models
{
    public class Options
    {
        public int OptionsID { get; set; }
        public string Name { get; set; }
        public bool Ended { get; set; }
    }
}
