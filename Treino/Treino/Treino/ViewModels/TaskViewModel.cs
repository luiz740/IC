﻿using Treino.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Treino.ViewModels
{
    public class TaskViewModel: BaseViewModel
    {
        public ObservableCollection<Options> Itens { get; set; }
        public Command AdicionarCommand { get; set; }
        public TaskViewModel()
        {
            Itens = new ObservableCollection<Options>();
            AdicionarCommand = new Command(ExecuteAdicionarCommand);
            Itens.Add(new Options
            {
                OptionsID = 1,
                Name = "Iniciar"
            });
            Itens.Add(new Options
            {
                OptionsID = 2,
                Name = "Pausar"
            });
            Itens.Add(new Options
            {
                OptionsID = 3,
                Name = "Parar"
            });
            Itens.Add(new Options
            {
                OptionsID = 4,
                Name = "#VoaInclude"
            });
        }

        private async void ExecuteAdicionarCommand()
        {
            await PushAsync<AdicionarViewModel>(); //throw new NotImplementedException();
        }
    }
}
