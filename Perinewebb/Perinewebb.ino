#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#include <TimeLib.h>
#include <NtpClientLib.h>
#include <ArduinoJson.h>

//#define ssid      "PerineWeb"        // WiFi SSID
//#define password  "esp826612e"    // WiFi password
#define ssid      "Chrys"        // WiFi SSID
#define password  "30911477"    // WiFi password
#define HISTORY_FILE "/history.json"
const uint8_t GPIOPIN[4] = {D5,D6,D7,D8};  // Led
float t = 0;
float   h = 0 ;
int flag = 0;
int flagtreino1 = 0;
int flagtreino2 = 0;
//float   pa = 0;
int     sizeHist = 6000 ;        // Taille historique (7h x 12pts) - History size  // número de pontos aceitável +/- 10 min

//const long intervalHist = 1000 * 60 * 5;  // 5 mesures / heure - 5 measures / hours
const long intervalHist = 1000;  // 1000ms = 1s de delay entre pontos
unsigned long previousMillis = intervalHist;  // Dernier point enregistré dans l'historique - time of last point added

ESP8266WebServer server ( 80 );

StaticJsonBuffer<10000> jsonBuffer;                 // Buffer static contenant le JSON courant - Current JSON static buffer
JsonObject& root = jsonBuffer.createObject();
JsonArray& timestamp = root.createNestedArray("timestamp");
JsonArray& hist_t = root.createNestedArray("t");
JsonArray& hist_h = root.createNestedArray("h");
JsonArray& hist_pa = root.createNestedArray("pa");
JsonArray& bart = root.createNestedArray("bart");   // Clé historgramme (temp/humidité) - Key histogramm (temp/humidity)
JsonArray& barh = root.createNestedArray("barh");   // Clé historgramme (temp/humidité) - Key histogramm (temp/humidity)

char json[10000];                                   // Buffer pour export du JSON - JSON export buffer

void updateGpio(){
  String gpio = server.arg("id");
  String etat = server.arg("etat");
  String success = "1";
  int pin = D5;
 if ( gpio == "D5" && etat == "1") {
      flagtreino1 = 1;
      flagtreino2 = 0;
      if ( etat == "0" ) {
      flagtreino1 = 0;
      flagtreino2 = 0;
    } else {
      success = "1";}
 } else if ( gpio == "D6" && etat == "1" ) {
    flagtreino1 = 0;
    flagtreino2 = 1;
    if ( etat == "0" ) {
      flagtreino1 = 0;
      flagtreino2 = 0;
    } else {
      success = "1";}
 } else if ( gpio == "D7" ) {
     pin = D7;
 } else if ( gpio == "D8" ) {
     pin = D8;  
 } else {   
      pin = D5;
  }
  Serial.println(pin);
  if ( etat == "1" ) {
    digitalWrite(pin, HIGH);
  } else if ( etat == "0" ) {
    digitalWrite(pin, LOW);
  } else {
    success = "1";
    Serial.println("Err Led Value");
  }
  
  String json = "{\"gpio\":\"" + String(gpio) + "\",";
  json += "\"etat\":\"" + String(etat) + "\",";
  json += "\"success\":\"" + String(success) + "\"}";
    
  server.send(200, "application/json", json);
  Serial.println("GPIO updated");
}

void sendHistory(){  
  root.printTo(json, sizeof(json));             // Export du JSON dans une chaine - Export JSON object as a string
  server.send(200, "application/json", json);   // Envoi l'historique au client Web - Send history data to the web client
  Serial.println("Send History");   
}

void loadHistory(){
  File file = SPIFFS.open(HISTORY_FILE, "r");
  if (!file){
    Serial.println("Aucun historique existe - No History Exist");
  } else {
    size_t size = file.size();
    if ( size == 0 ) {
      Serial.println("Fichier historique vide - History file empty !");
    } else {
      std::unique_ptr<char[]> buf (new char[size]);
      file.readBytes(buf.get(), size);
      JsonObject& root = jsonBuffer.parseObject(buf.get());
      if (!root.success()) {
        Serial.println("Impossible de lire le JSON - Impossible to read JSON file");
      } else {
        Serial.println("Historique charge - History loaded");
        root.prettyPrintTo(Serial);  
      }
    }
    file.close();
  }
}

void saveHistory(){
  Serial.println("Save History");            
  File historyFile = SPIFFS.open(HISTORY_FILE, "w");
  root.printTo(historyFile); // Exporte et enregsitre le JSON dans la zone SPIFFS - Export and save JSON object to SPIFFS area
  historyFile.close();  
}

void setup() {
  NTP.onNTPSyncEvent([](NTPSyncEvent_t error) {
    if (error) {
      Serial.print("Time Sync error: ");
      if (error == noResponse)
        Serial.println("NTP server not reachable");
      else if (error == invalidAddress)
        Serial.println("Invalid NTP server address");
      }
    else {
      Serial.print("Got NTP time: ");
      Serial.println(NTP.getTimeDateString(NTP.getLastNTPSync()));
    }
  });
  NTP.begin("pool.ntp.org", 1, true, 0);
  NTP.setInterval(63);
  delay(50);
     
  for ( int x = 0 ; x < 5 ; x++ ) {
    pinMode(GPIOPIN[x], OUTPUT);
  }
  
  Serial.begin ( 115200 );
  WiFi.begin ( ssid, password );
  int tentativeWiFi = 0;
  // Attente de la connexion au réseau WiFi / Wait for connection
  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 ); Serial.print ( "." );
    tentativeWiFi++;
    if ( tentativeWiFi > 20 ) {
      ESP.reset();
      while(true)
        delay(1);
    }
  }
  // Connexion WiFi établie / WiFi connexion is OK
  Serial.println ( "" );
  Serial.print ( "Connected to " ); Serial.println ( ssid );
  Serial.print ( "IP address: " ); Serial.println ( WiFi.localIP() );
  
  if (!SPIFFS.begin()) {
    Serial.println("SPIFFS Mount failed");        // Problème avec le stockage SPIFFS - Serious problem with SPIFFS 
  } else { 
    Serial.println("SPIFFS Mount succesfull");
    loadHistory();
  }
  delay(50);
  
  server.on("/gpio", updateGpio);
  server.on("/graph_temp.json", sendHistory);

  server.serveStatic("/js", SPIFFS, "/js");
  server.serveStatic("/css", SPIFFS, "/css");
  server.serveStatic("/img", SPIFFS, "/img");
  server.serveStatic("/", SPIFFS, "/index.html");

  server.begin();
  Serial.println ( "HTTP server started" );

  Serial.print("Uptime :");
  Serial.println(NTP.getUptime());
  Serial.print("LastBootTime :");
  Serial.println(NTP.getLastBootTime());
}

void loop() {
  // put your main code here, to run repeatedly:

  float t1 = analogRead(A0);
  server.handleClient();
  t = 0.5616*t1 -27;//-15;
  //t = 0.180444*t1 - 25.0974;
  if ( isnan(t) ) {
    //Erreur, aucune valeur valide - Error, no valid value
  } else {
    addPtToHist();
  }
  delay(50);
}

void addPtToHist(){
  unsigned long currentMillis = millis();
  if ( currentMillis - previousMillis > intervalHist ) {
    long int tps = NTP.getTime();
    previousMillis = currentMillis;
    if (flagtreino2 == 1){
      delay(1000);
      timestamp.removeAt(0);
        hist_t.removeAt(0);
        hist_h.removeAt(0);
        hist_pa.removeAt(0);
        delay(50);
      // primeiro intervalo
      timestamp.add(tps);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      timestamp.add(tps+20);      
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      timestamp.add(tps+30);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(8, 1));
      timestamp.add(tps+40);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
       
      //segundo intervalo

       timestamp.add(tps+40);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      timestamp.add(tps+50);      
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      timestamp.add(tps+60);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(8, 1));
      timestamp.add(tps+70);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));

      //terceiro intervalo
      timestamp.add(tps+70);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      timestamp.add(tps+80);      
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      timestamp.add(tps+90);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(8, 1));
      timestamp.add(tps+100);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      timestamp.add(tps-1);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      flag = 1;
      flagtreino1 = 0;
      flagtreino2 = 0;
    }
    if (flagtreino1 == 1){
      delay(1000);
      timestamp.removeAt(0);
        hist_t.removeAt(0);
        hist_h.removeAt(0);
        hist_pa.removeAt(0);
        delay(50);
      // primeiro intervalo
      timestamp.add(tps);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      timestamp.add(tps+20);      
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      timestamp.add(tps+20);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(5, 1));
      timestamp.add(tps+25);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(5, 1));
      timestamp.add(tps+25);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(5, 1));
      timestamp.add(tps+25);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
       
      //segundo intervalo

      timestamp.add(tps+28);      
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      timestamp.add(tps+28);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(5, 1));
      timestamp.add(tps+28);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(5, 1));
      timestamp.add(tps+33);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(5, 1));
      timestamp.add(tps+33);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));

      //terceiro intervalo
      
      timestamp.add(tps+36);      
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      timestamp.add(tps+36);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(5, 1));
      timestamp.add(tps+36);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(5, 1));
      timestamp.add(tps+41);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(5, 1));
      timestamp.add(tps+41);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      timestamp.add(tps-1);
      hist_t.add(double_with_n_digits(0, 1));
      hist_pa.add(double_with_n_digits(0, 1));
      hist_h.add(double_with_n_digits(0, 1));
      flag = 1;
      flagtreino1 = 0;
      flagtreino2 = 0;
    }
    else if ( tps > 0 && tps < 1743103718 && flag == 1) { // impedir pulo para 2036 <-- Bug 1743103718 = 19:28:38 27/03/2025
      timestamp.add(tps-1); //tps
      hist_t.add(double_with_n_digits(t, 1));
      hist_pa.add(double_with_n_digits(t, 1));
      if ( hist_t.size() > sizeHist ) {
        timestamp.removeAt(0);
        hist_t.removeAt(0);
        hist_h.removeAt(0);
        hist_pa.removeAt(0);
      }
      delay(10);
      saveHistory();
    }  
  }
}
