﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PerineApp.ViewModels
{
    class RegisterViewModel: BaseViewModel
    {
        public Command BackCommand { get; set; }
        public RegisterViewModel()
        {
            BackCommand = new Command(ExecuteBackCommand);
        }

        private async void ExecuteBackCommand()
        {
            await PushAsync<MainViewModel>();
        }
    }
}
