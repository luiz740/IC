﻿using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PerineApp.ViewModels
{
    public class PmaxViewModel: BaseViewModel
    {
        public PlotModel PmaxModel { get; set; }
        public Command AdicionarCommand { get; set; }
        int cont = 0;
        int flag = 0;
        int temp = 10;
        int x = 10;
        string k;

        public PmaxViewModel()
        {
            AdicionarCommand = new Command(ExecuteAdicionarCommand);
            CategoryAxis xaxis = new CategoryAxis();
            xaxis.Position = AxisPosition.Bottom;
            xaxis.MajorGridlineStyle = LineStyle.Solid;
            xaxis.MinorGridlineStyle = LineStyle.Dot;
            xaxis.Labels.Add(" - ");

            LinearAxis yaxis = new LinearAxis();
            yaxis.Position = AxisPosition.Left;
            // desabilitar o zoom
            yaxis.IsZoomEnabled = false;
            yaxis.IsPanEnabled = false;
            xaxis.IsZoomEnabled = false;
            xaxis.IsPanEnabled = false;
            yaxis.Minimum = 0;
            yaxis.Maximum = 200;
            yaxis.MajorGridlineStyle = LineStyle.Dot;
            xaxis.MinorGridlineStyle = LineStyle.Dot;

            ColumnSeries s1 = new ColumnSeries();
            s1.IsStacked = true;
            PmaxModel = new PlotModel();
            PmaxModel.Title = "Teste de Força";
            //PmaxModel.Background = OxyColors.White;
            PmaxModel.Axes.Add(xaxis);
            PmaxModel.Axes.Add(yaxis);
        }
        private void ExecuteAdicionarCommand()
        {
            ColumnSeries s1 = new ColumnSeries();
            s1.IsStacked = true;
            k = cont.ToString();
            PmaxModel.Annotations.Clear();
            PmaxModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(0, 165), Text = k, FontSize = 50 });

            if (cont < 50)
            {
                PmaxModel.Series.Clear();
                cont = cont + 9;
                s1.Items.Add(new ColumnItem { Value = cont, CategoryIndex = 0, Color = OxyPlot.OxyColors.Green });
                PmaxModel.Series.Add(s1);
            }
            if (cont < 100 && cont >= 50)
            {
                PmaxModel.Series.Clear();
                cont = cont + 4;
                s1.Items.Add(new ColumnItem { Value = 50, CategoryIndex = 0, Color = OxyPlot.OxyColors.Green });
                s1.Items.Add(new ColumnItem { Value = cont - 50, CategoryIndex = 0, Color = OxyPlot.OxyColors.Yellow });
                PmaxModel.Series.Add(s1);
            }
            if (cont < 150 && cont >= 100)
            {
                PmaxModel.Series.Clear();
                cont = cont + 1;
                s1.Items.Add(new ColumnItem { Value = 50, CategoryIndex = 0, Color = OxyPlot.OxyColors.Green });
                s1.Items.Add(new ColumnItem { Value = 100 - 50, CategoryIndex = 0, Color = OxyPlot.OxyColors.Yellow });
                s1.Items.Add(new ColumnItem { Value = cont - 100, CategoryIndex = 0, Color = OxyPlot.OxyColors.Red });
                PmaxModel.Series.Add(s1);
                //PmaxModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(0, 165), Text = "10", FontSize = 50 });
            }
            //if (cont >= 100 && flag == 0)
            //{
            //    Tempo();
            //    flag = flag + 1;
            //}
            PmaxModel.InvalidatePlot(true);
            cont = cont + 1;
        }
        //public async void Tempo()
        //{
        //    k = cont.ToString();
        //    PmaxModel.Annotations.Clear();
        //    PmaxModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(0, 165), Text = k, FontSize = 50 });
        //    PmaxModel.InvalidatePlot(true);
        //    temp = temp - 1;
        //    if (temp != 0)
        //    {
        //        await Task.Delay(1000);
        //        Tempo();
        //    }
        //    else
        //    {
        //        await Task.Delay(1000);
        //        k = temp.ToString();
        //        PmaxModel.Annotations.Clear();
        //        PmaxModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(0, 165), Text = k, FontSize = 50 });
        //        PmaxModel.InvalidatePlot(true);
        //    }
        //}
    }
}
