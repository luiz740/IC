﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PerineApp.ViewModels
{
    class MainViewModel: BaseViewModel
    {
        public Command PmaxCommand { get; set; }
        public Command PrimeiroTreinoCommand { get; set; }
        public Command SegundoTreinoCommand { get; set; }
        public Command RegisterCommand { get; set; }

        public MainViewModel()
        {
            PmaxCommand = new Command(ExecutePmaxCommand);
            PrimeiroTreinoCommand = new Command(ExecutePrimeiroTreinoCommand);
            SegundoTreinoCommand = new Command(ExecuteSegundoTreinoCommand);
            RegisterCommand = new Command(ExecuteRegisterCommand);
        }

        private async void ExecuteRegisterCommand()
        {
            await PushAsync<RegisterViewModel>();
        }

        private async void ExecutePrimeiroTreinoCommand()
        {
            await PushAsync<PrimeiroTreinoViewModel>();
        }

        private async void ExecutePmaxCommand()
        {
            await PushAsync<PmaxViewModel>();
        }
        private async void ExecuteSegundoTreinoCommand()
        {
            await PushAsync<SegundoTreinoViewModel>();
        }
    }
}
