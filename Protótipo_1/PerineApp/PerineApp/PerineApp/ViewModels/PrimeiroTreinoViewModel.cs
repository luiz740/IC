﻿using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PerineApp.ViewModels
{
    class PrimeiroTreinoViewModel: BaseViewModel
    {
        public PlotModel PrimeiroTreinoModel { get; set; }
        public Command AdicionarCommand { get; set; }
        int cont = 0;
        int pmax = 0;
        int flag = 0;
        int flagfinal = 0;        
        int temp = 5;
        int x = 10;
        int serie = 0;
        string k;
        string flag1;
        string k1;
        string kpmax;

        public PrimeiroTreinoViewModel()
        {
            AdicionarCommand = new Command(ExecuteAdicionarCommand);
            CategoryAxis xaxis = new CategoryAxis();
            xaxis.Position = AxisPosition.Bottom;
            xaxis.MajorGridlineStyle = LineStyle.Solid;
            xaxis.MinorGridlineStyle = LineStyle.Dot;
            xaxis.Labels.Add(" ");
            xaxis.Labels.Add(" ");
            xaxis.Labels.Add(" ");
            xaxis.Labels.Add(" ");

            LinearAxis yaxis = new LinearAxis();
            yaxis.Position = AxisPosition.Left;
            yaxis.Minimum = 0;
            yaxis.Maximum = 200;
            // limitar o gráfico
            yaxis.AbsoluteMaximum = 250;
            yaxis.AbsoluteMinimum = 0;
            // limita embaixo no inicio, no 0 ele corta a primeira coluna pela metade
            xaxis.AbsoluteMinimum = -0.5;
            yaxis.MajorGridlineStyle = LineStyle.Dot;
            xaxis.MinorGridlineStyle = LineStyle.Dot;

            ColumnSeries s1 = new ColumnSeries();
            s1.IsStacked = true;
            PrimeiroTreinoModel = new PlotModel();
            PrimeiroTreinoModel.Title = "Teste de Força";
            //PrimeiroTreinoModel.Background = OxyColors.White;
            PrimeiroTreinoModel.Axes.Add(xaxis);
            PrimeiroTreinoModel.Axes.Add(yaxis);

            ////////////////////////////////////////// serie 3 de informações /////////////////////////////////////////////////////////////
            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 165), Text = k1, FontSize = 30 });
            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 190), Text = "P", FontSize = 15 });

            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 115), Text = kpmax, FontSize = 30 });
            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 140), Text = "Pmax", FontSize = 15 });

            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 65), Text = "A", FontSize = 30 });
            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 90), Text = "A", FontSize = 15 });

            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 15), Text = "A", FontSize = 30 });
            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 40), Text = "A", FontSize = 15 });
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }
        private void Annotation()
        {
            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 165), Text = k1, FontSize = 30 });
            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 190), Text = "P", FontSize = 15 });

            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 115), Text = kpmax, FontSize = 30 });
            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 140), Text = "Pmax", FontSize = 15 });

            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 65), Text = "A", FontSize = 30 });
            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 90), Text = "A", FontSize = 15 });

            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 15), Text = "A", FontSize = 30 });
            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(3, 40), Text = "A", FontSize = 15 });

            PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(serie, 165), Text = k, FontSize = 30 });
        }
        private void ExecuteAdicionarCommand()
        {
            ColumnSeries s1 = new ColumnSeries();
            s1.IsStacked = true;
            kpmax = pmax.ToString();
            k1 = cont.ToString();
            PrimeiroTreinoModel.Annotations.Clear();
            Annotation();
            if(pmax < cont)
            {
                pmax = cont;
                kpmax = pmax.ToString();
            }
            if (cont < 50)
            {
                // inicio da contagem
                if (flag == 0)
                {
                    Tempo();
                    flag = flag + 1;
                }
                PrimeiroTreinoModel.Series.Clear();
                cont = cont + 9;
                s1.Items.Add(new ColumnItem { Value = cont, CategoryIndex = serie, Color = OxyPlot.OxyColors.Green });
                PrimeiroTreinoModel.Series.Add(s1);
            }
            if (cont < 100 && cont >= 50)
            {
                PrimeiroTreinoModel.Series.Clear();
                cont = cont + 4;
                s1.Items.Add(new ColumnItem { Value = 50, CategoryIndex = serie, Color = OxyPlot.OxyColors.Green });
                s1.Items.Add(new ColumnItem { Value = cont - 50, CategoryIndex = serie, Color = OxyPlot.OxyColors.Yellow });
                PrimeiroTreinoModel.Series.Add(s1);
            }
            if (cont < 150 && cont >= 100)
            {
                PrimeiroTreinoModel.Series.Clear();
                cont = cont + 1;
                s1.Items.Add(new ColumnItem { Value = 50, CategoryIndex = serie, Color = OxyPlot.OxyColors.Green });
                s1.Items.Add(new ColumnItem { Value = 100 - 50, CategoryIndex = serie, Color = OxyPlot.OxyColors.Yellow });
                s1.Items.Add(new ColumnItem { Value = cont - 100, CategoryIndex = serie, Color = OxyPlot.OxyColors.Red });
                PrimeiroTreinoModel.Series.Add(s1);
            }
            if (serie == 2 && flagfinal >= 2)
            {
                PrimeiroTreinoModel.Series.Clear();
                cont = 0;
            }

            PrimeiroTreinoModel.InvalidatePlot(true);
            cont = cont + 1;
        }
        public async void Tempo()
        {
            k = temp.ToString();
            temp = temp - 1;
            PrimeiroTreinoModel.Annotations.Clear();
            Annotation();
            PrimeiroTreinoModel.InvalidatePlot(true);
            //temp = temp - 1;
            if (temp != 0)
            {
                await Task.Delay(1000);
                Tempo();
            }
            else
            {
                await Task.Delay(1000);
                k = temp.ToString();
                PrimeiroTreinoModel.Annotations.Clear();
                Annotation();
                PrimeiroTreinoModel.InvalidatePlot(true);
                await Task.Delay(1000);
                temp = 5;
                Espera();
                
            }
        }
        public async void Espera()
        {
            if (serie == 2)
            {
                flagfinal = flagfinal + 1;
            }

            if (serie < 2)
            {
                k = temp.ToString();
                PrimeiroTreinoModel.Series.Clear();
                PrimeiroTreinoModel.Annotations.Clear();
                Annotation();
                PrimeiroTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(serie, 190), Text = "Espere", FontSize = 20 });
                PrimeiroTreinoModel.InvalidatePlot(true);
                temp = temp - 1;
                cont = 0;
            }
            if (temp != 0)
            {
                await Task.Delay(1000);
                Espera();
            }
            else
            {
                await Task.Delay(1000);
                k = temp.ToString();
                PrimeiroTreinoModel.Annotations.Clear();
                Annotation();
                PrimeiroTreinoModel.InvalidatePlot(true);
                if (serie < 2) {
                    serie = serie + 1;
                    cont = 0;
                    flag = 0;
                    temp = 5;
                    PrimeiroTreinoModel.InvalidatePlot(true);
                    await Task.Delay(1000);
                    PrimeiroTreinoModel.Annotations.Clear();
                    Annotation();
                    PrimeiroTreinoModel.InvalidatePlot(true);
                    if (serie == 2)
                    {
                        flagfinal = flagfinal + 1;
                    }
                }                
            }
        }
    }
}
