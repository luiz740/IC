﻿using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PerineApp.ViewModels
{
    class SegundoTreinoViewModel: BaseViewModel
    {
        public PlotModel SegundoTreinoModel { get; set; }
        public Command AdicionarCommand { get; set; }
        public Command RemoverCommand { get; set; }
        public Command ResetCommand { get; set; }
        int conty = 0;
        int contx = 0;
        int pmax1 = 0;
        int pmax2 = 0;
        int xmax1 = 0;
        int xmax2 = 0;
        string kpmax1;
        string kpmax2;
        int i = 0;
        int[] x = new int[999];
        int[] y = new int[999];
        int minX = 0;
        int maxX = 100;
        int minY = 0;
        int maxY = 200;

        public SegundoTreinoViewModel()
        {
            AdicionarCommand = new Command(ExecuteAdicionarCommand);
            RemoverCommand = new Command(ExecuteRemoverCommand);
            ResetCommand = new Command(ExecuteResetCommand);

            LinearAxis xaxis = new LinearAxis();
            xaxis.Position = AxisPosition.Bottom;
            xaxis.Minimum = minX;
            xaxis.Maximum = maxX;
            xaxis.AbsoluteMinimum = 0;
            xaxis.MajorGridlineStyle = LineStyle.Dot;

            LinearAxis yaxis = new LinearAxis();
            yaxis.Position = AxisPosition.Left;
            yaxis.Minimum = minY;
            yaxis.Maximum = maxY;
            // limitar o gráfico
            yaxis.AbsoluteMaximum = 250;
            yaxis.AbsoluteMinimum = 0;
            yaxis.MajorGridlineStyle = LineStyle.Dot;

            LineSeries s1 = new LineSeries()
            {               
                //MarkerType = MarkerType.Circle,
                //MarkerSize = 6,
                //MarkerStroke = OxyColors.Black,
                //MarkerFill = OxyColors.SkyBlue,
                //MarkerStrokeThickness = 5
            };
            //tamanho da linha
            s1.StrokeThickness = 5;
            //cor da linha
            s1.Color = OxyColors.Red;
            s1.Points.Add(new DataPoint(0, 0));
            s1.Points.Add(new DataPoint(30, 0));
            s1.Points.Add(new DataPoint(60, 100));
            s1.Points.Add(new DataPoint(60, 0));
            s1.Points.Add(new DataPoint(90, 0));
            s1.Points.Add(new DataPoint(120, 100));
            s1.Points.Add(new DataPoint(120, 0));
            s1.Points.Add(new DataPoint(150, 0));

            SegundoTreinoModel = new PlotModel();
            SegundoTreinoModel.Title = "Teste de Força";
            SegundoTreinoModel.Axes.Add(xaxis);
            SegundoTreinoModel.Axes.Add(yaxis);
            SegundoTreinoModel.Series.Add(s1);
        }

        private void ExecuteResetCommand()
        {
            conty = 0;
            SegundoTreinoModel.InvalidatePlot(true);
        }

        private void treino()
        {
            LineSeries s1 = new LineSeries()
            {
                //MarkerType = MarkerType.Circle,
                //MarkerSize = 6,
                //MarkerStroke = OxyColors.Black,
                //MarkerFill = OxyColors.SkyBlue,
                //MarkerStrokeThickness = 5
            };
            //tamanho da linha
            s1.StrokeThickness = 5;
            s1.Color = OxyColors.Red;
            s1.Points.Add(new DataPoint(0, 0));
            s1.Points.Add(new DataPoint(30, 0));
            s1.Points.Add(new DataPoint(60, 100));
            s1.Points.Add(new DataPoint(60, 0));
            s1.Points.Add(new DataPoint(90, 0));
            s1.Points.Add(new DataPoint(120, 100));
            s1.Points.Add(new DataPoint(120, 0));
            s1.Points.Add(new DataPoint(150, 0));
            SegundoTreinoModel.Series.Add(s1);
        }

        private void ExecuteRemoverCommand()
        {
            i++;
            LineSeries s1 = new LineSeries();


            if (contx > 30 && contx < 71 && pmax1 < conty)
            {
                SegundoTreinoModel.Annotations.Clear();
                pmax1 = conty;
                xmax1 = contx;
                kpmax1 = pmax1.ToString();
                //SegundoTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(xmax1, pmax1), Text = kpmax1, FontSize = 10 });
                SegundoTreinoModel.Annotations.Add(new PointAnnotation { Y = pmax1, X = xmax1, Text = kpmax1, Shape = MarkerType.Circle, Fill = OxyColors.White, Stroke = OxyColors.Black, StrokeThickness = 2, FontSize = 20, FontWeight = 3 });
            }
            if (contx > 90 && contx < 131 && pmax2 < conty)
            {
                SegundoTreinoModel.Annotations.Clear();
                pmax2 = conty;
                xmax2 = contx;
                kpmax2 = pmax2.ToString();
                //SegundoTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(xmax1, pmax1), Text = kpmax1, FontSize = 10 });
                SegundoTreinoModel.Annotations.Add(new PointAnnotation { Y = pmax1, X = xmax1, Text = kpmax1, Shape = MarkerType.Circle, Fill = OxyColors.White, Stroke = OxyColors.Black, StrokeThickness = 2, FontSize = 20, FontWeight = 3 });
                SegundoTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(xmax2, pmax2), Text = kpmax2, FontSize = 20 });
            }


            SegundoTreinoModel.Series.Clear();
            conty = conty - 3;
            y[i] = conty;                               //vetor de inteiros para memorizar os valores do contador

            contx = contx + 2;
            x[i] = contx;                               //vetor de inteiros para memorizar os valores do contador
            treino();
            pontos();

        }

        private void ExecuteAdicionarCommand()
        {
            i++;
            LineSeries s1 = new LineSeries();
            

            if (contx > 30 && contx < 71 && pmax1 < conty)
            {
                SegundoTreinoModel.Annotations.Clear();
                pmax1 = conty;
                xmax1 = contx;
                kpmax1 = pmax1.ToString();
                //SegundoTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(xmax1, pmax1), Text = kpmax1, FontSize = 10 });
                SegundoTreinoModel.Annotations.Add(new PointAnnotation { Y = pmax1, X = xmax1, Text = kpmax1, Shape = MarkerType.Circle, Fill = OxyColors.White, Stroke = OxyColors.Black, StrokeThickness = 2, FontSize = 20, FontWeight = 3 });
            }
            if (contx > 90 && contx < 131 && pmax2 < conty)
            {
                SegundoTreinoModel.Annotations.Clear();
                pmax2 = conty;
                xmax2 = contx;
                kpmax2 = pmax2.ToString();
                //SegundoTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(xmax1, pmax1), Text = kpmax1, FontSize = 10 });
                SegundoTreinoModel.Annotations.Add(new PointAnnotation { Y = pmax1, X = xmax1, Text = kpmax1, Shape = MarkerType.Circle, Fill = OxyColors.White, Stroke = OxyColors.Black, StrokeThickness = 2, FontSize = 20, FontWeight = 3 });
                SegundoTreinoModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(xmax2, pmax2), Text = kpmax2, FontSize = 20 });
            }

            SegundoTreinoModel.Series.Clear();
            conty = conty + 5;
            y[i] = conty;                               //vetor de inteiros para memorizar os valores do contador
            

            contx = contx + 2;
            x[i] = contx;                               //vetor de inteiros para memorizar os valores do contador
            treino();
            pontos();
                                          
        }

        private void pontos()
        {
            LinearAxis xaxis = new LinearAxis();
            xaxis.Position = AxisPosition.Bottom;
            xaxis.Minimum = minX;
            xaxis.Maximum = maxX;
            xaxis.AbsoluteMinimum = 0;
            xaxis.AbsoluteMaximum = 200;
            xaxis.MajorGridlineStyle = LineStyle.Dot;

            LinearAxis yaxis = new LinearAxis();
            yaxis.Position = AxisPosition.Left;
            yaxis.Minimum = minY;
            yaxis.Maximum = maxY;
            // limitar o gráfico
            yaxis.AbsoluteMaximum = 250;
            yaxis.AbsoluteMinimum = 0;
            yaxis.MajorGridlineStyle = LineStyle.Dot;

            LineSeries s1 = new LineSeries();
            s1.Points.Add(new DataPoint(0, 0));
            for (int j = 0; j < 300; j++)
            {
                if (y[j] != 0)
                s1.Points.Add(new DataPoint(x[j], y[j]));
            }
            if(maxX < contx + 50 && contx <= 150)
            {
                maxX = contx + 50;
                minX = contx - 50;
            }
            SegundoTreinoModel.Axes.Clear();
            SegundoTreinoModel.InvalidatePlot(true);
            SegundoTreinoModel.Axes.Add(xaxis);
            SegundoTreinoModel.Axes.Add(yaxis);
            SegundoTreinoModel.Series.Add(s1);
        }
    }
}
