﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PerineApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PmaxPage : ContentPage
    {
        public PmaxPage()
        {
            InitializeComponent();
            BindingContext = new ViewModels.PmaxViewModel();
        }
    }
}