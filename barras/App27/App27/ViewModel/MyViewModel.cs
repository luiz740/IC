﻿using Java.Lang;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App27.ViewModel
{
    public class MyViewModel
    {
        
        public PlotModel MyModel { get; set; }
        public Command AdicionarCommand { get; set; }
        int cont = 0;
        int flag = 0;
        int temp = 10;
        int x = 10;
        string k;
        public MyViewModel()
        {
            AdicionarCommand = new Command(ExecuteAdicionarCommand);

            CategoryAxis xaxis = new CategoryAxis();
            xaxis.Position = AxisPosition.Bottom;
            xaxis.MajorGridlineStyle = LineStyle.Solid;
            xaxis.MinorGridlineStyle = LineStyle.Dot;
            xaxis.Labels.Add(" - ");

            LinearAxis yaxis = new LinearAxis();
            yaxis.Position = AxisPosition.Left;
            yaxis.Minimum = 0;
            yaxis.Maximum = 200;
            yaxis.MajorGridlineStyle = LineStyle.Dot;
            xaxis.MinorGridlineStyle = LineStyle.Dot;

            ColumnSeries s1 = new ColumnSeries();
            s1.IsStacked = true;

            MyModel = new PlotModel();
            MyModel.Title = "Teste de Força";
            //MyModel.Background = OxyColors.White;

            MyModel.Axes.Add(xaxis);
            MyModel.Axes.Add(yaxis);
            //MyModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(0, 165), Text = "10", FontSize = 50 });
            //MyModel.Annotations.Add(new LineAnnotation { Slope = 0.1, Intercept = 50, Text = "First" });
            //MyModel.Annotations.Add(new PointAnnotation { Y = 60, Text = "Circle", Shape = MarkerType.Circle, Fill = OxyColors.LightGray, Stroke = OxyColors.DarkGray, StrokeThickness = 1 });
            //MyModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(0, 60), Text = "TextAnnotation" });
        }        
        private void ExecuteAdicionarCommand()
        {
            ColumnSeries s1 = new ColumnSeries();
            s1.IsStacked = true;
            if (cont < 50)
            {
                MyModel.Series.Clear();
                cont = cont + 9;
                s1.Items.Add(new ColumnItem { Value = cont, CategoryIndex = 0, Color = OxyPlot.OxyColors.Green });
                MyModel.Series.Add(s1);
            }
            if (cont < 100 && cont >= 50)
            {
                MyModel.Series.Clear();
                cont = cont + 4;
                s1.Items.Add(new ColumnItem { Value = cont, CategoryIndex = 0, Color = OxyPlot.OxyColors.Yellow });
                MyModel.Series.Add(s1);
            }
            if (cont < 150 && cont >= 100)
            {
                MyModel.Series.Clear();
                cont = cont + 1;
                s1.Items.Add(new ColumnItem { Value = cont, CategoryIndex = 0, Color = OxyPlot.OxyColors.Red });
                MyModel.Series.Add(s1);                
              //MyModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(0, 165), Text = "10", FontSize = 50 });
            }
            if(cont>= 100 && flag == 0)
            {
                Tempo();
                flag = flag + 1;
            }
            MyModel.InvalidatePlot(true);
            cont = cont + 1;
        }
        public async void Tempo()
        {
            k = temp.ToString();
            MyModel.Annotations.Clear();
            MyModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(0, 165), Text = k, FontSize = 50 });
            MyModel.InvalidatePlot(true);
            temp = temp - 1;
            if (temp != 0)
            {
                await Task.Delay(1000);
                Tempo();
            }
            else
            {
                await Task.Delay(1000);
                k = temp.ToString();
                MyModel.Annotations.Clear();
                MyModel.Annotations.Add(new TextAnnotation { TextPosition = new DataPoint(0, 165), Text = k, FontSize = 50 });
                MyModel.InvalidatePlot(true);
            }
        }        
    }
}
